/****************************************************************************
 * arch/arm/src/bcm2837b0/bcm2837b0_armtimer_isr.c
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <stdint.h>
#include <time.h>

#include <nuttx/arch.h>
#include <arch/irq.h>

#include "chip.h"
#include "bcm2837b0_map.h"
#include "up_arch.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/





/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Function:  bcm2837b0_armtimer_isr
 *
 * Description:
 *   The timer ISR will perform a variety of services for various portions
 *   of the systems.
 *
 ****************************************************************************/

static int bcm2837b0_armtimer_isr(int irq, uint32_t *regs, FAR void *arg)
{
  /* Clear ARM Timer IRQ */
  putreg32(0, BCM2837B0_ARM_TIMER_CLEAR_ACK_REG);

  /* Process timer interrupt event */

  sched_process_timer();
  return OK;
}

static void bcm2837b0_armtimer_init(void)
{
  putreg32(0, BCM2837B0_ARM_TIMER_CTRL_REG);
}

static void bcm2837b0_armtimer_pre_scaler_set(uint32_t pre_scaler)
{
  /* 
   * 32bit Time Control Register
   * Bit(s) 23:16 pre-scaler
   * Bit(s) 9     Free Running Counter Enabled
   * Bit(s) 8     Timer halted if ARM is in debug halted mode
   * Bit(s) 7     Timer enabled
   * Bit(s) 5     Timer interrupt enabled
   * Bit(s) 1     0:16-bit counter; 1:32-bit counter
   * Bit(s) 0     Not used
   */
  putreg32((pre_scaler << 16) | getreg32(BCM2837B0_ARM_TIMER_CTRL_REG), BCM2837B0_ARM_TIMER_CTRL_REG);
 
}

static void bcm2837b0_armtimer_free_running_counter_set(uint32_t fr_cnt)
{
   putreg32(fr_cnt, BCM2837B0_ARM_TIMER_LOAD_REG);
}

static void bcm2837b0_armtimer_start_running()
{
   putreg32(((0x1 << 9) | (0x1 << 7) | (0x1 << 5) | (0x1 << 1)) | getreg32(BCM2837B0_ARM_TIMER_CTRL_REG), BCM2837B0_ARM_TIMER_CTRL_REG);
}


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Function:  arm_timer_initialize
 *
 * Description:
 *   This function is called during start-up to initialize
 *   the timer interrupt.
 *
 ****************************************************************************/
#define NUTTX_TICK_RATE_HZ 1000
void arm_timer_initialize(void)
{

  /* Disable interrupts */

  up_disable_irq(BCM_IRQ_SYSTIMER);

  /*
   * Use sys_clk as the clock source, no from the apb_clk.
   * As the sys_clk will change base on the system performance changing.
   * Using the sys_clk as the Operating System tick timer, OS scheduler performance will change.
   *
   * Free Running Counter Frequency = sys_clk / (pre-scaler + 1)
   * As we would like setting the OS tick to 1ms, so the OS tick interrupt Frequency is 1000 Hz
   * sys_clk = configCPU_CLOCK_HZ = 240 MHz;
   * os_tick = configTICK_RATE_HZ = 1000 Hz
   *
   * !!!REMINDER!!!: The OS tick Frequency is not equal to the Free Running Counter Frequency.
   * And more, the pre-scaler is only 8 bit (255 in max), sys_clk is 240 MHz (240000000).
   * Free Running Counter Frequency is 1 MHz (100000 Hz) in minimum.
   * OS tick Frequency = Free Running Counter Frequency / (Count Down Load Value + 1) .
   * 
   * pre-scaler = 240 - 1 = 0xEF;
   * Count Down Load Value = 1000 - 1;
   */
  bcm2837b0_armtimer_init();
  bcm2837b0_armtimer_pre_scaler_set((uint32_t)(BCM2837B0_SYSCLK / 1000000) - 1);
  bcm2837b0_armtimer_free_running_counter_set((uint32_t)(1000000 / NUTTX_TICK_RATE_HZ) - 1);
  bcm2837b0_armtimer_start_running();

  /* Attach the timer interrupt vector */

  (void)irq_attach(BCM_IRQ_SYSTIMER, (xcpt_t)bcm2837b0_armtimer_isr, NULL);

  /* And enable the timer interrupt */

  up_enable_irq(BCM_IRQ_SYSTIMER);
}

