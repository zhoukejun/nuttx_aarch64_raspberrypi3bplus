/****************************************************************************
 * arch/arm/src/bcm2837b0/bcm2837b0_fiq.c
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <assert.h>

#include <nuttx/arch.h>

#include "up_internal.h"
#include "sctlr.h"
#include "up_arch.h"
#include "bcm2837b0_irq.h"
#include "bcm2837b0_map.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/


/****************************************************************************
 * Public Data
 ****************************************************************************/


/****************************************************************************
 * Public Functions
 ****************************************************************************/


/****************************************************************************
 * Name: up_enable_fiq
 *
 * Description:
 *   On many architectures, there are three levels of interrupt enabling: (1)
 *   at the global level, (2) at the level of the interrupt controller,
 *   and (3) at the device level.  In order to receive interrupts, they
 *   must be enabled at all three levels.
 *
 *   This function implements enabling of the device specified by 'fiq'
 *   at the interrupt controller level if supported by the architecture
 *   (up_fiq_restore() supports the global level, the device level is hardware
 *   specific).
 *
 *   Since this API is not supported on all architectures, it should be
 *   avoided in common implementations where possible.
 *
 ****************************************************************************/

void up_enable_fiq(int fiq)
{
}

/****************************************************************************
 * Name: up_disable_fiq
 *
 * Description:
 *   This function implements disabling of the device specified by 'fiq'
 *   at the fast interrupt controller level if supported by the architecture
 *   (up_fiq_save() supports the global level, the device level is hardware
 *   specific).
 *
 *   Since this API is not supported on all architectures, it should be
 *   avoided in common implementations where possible.
 *
 ****************************************************************************/

void up_disable_fiq(int fiq)
{
}

/****************************************************************************
 * Name: bcm2837b0_get_fiq_id
 *
 * Description:
 *   This function check the IRQ Pending registers to get the FIQ Id.
 *
 *  Input Parameters:
 *  void
 *
 ****************************************************************************/

int bcm2837b0_get_firq_id(void)
{
  int fiq;

#if 0
  uint32_t fiq_pending = getreg32(BCM2837B0_IRQ_BASIC_PENDING_REG);

  // Bits 7 ~ 0 in IRQBASIC_PENDING represent interrupts :
  if (irq_basic_pending & 0xFF)
  {
    irq = 31 - __builtin_clz(irq_basic_pending);
  }

  // Bit 8 in IRQ_BASSIC_PENDING indicates interrupts in Pending1 (interrupts 31-0):
  if (irq_basic_pending & (1UL << 8))
  {
    uint32_t per_pending_1 = getreg32(BCM2837B0_IRQ_PENDING_1_REG);
    irq = BCM_BASIC_IRQ_TOTAL + 31 - __builtin_clz(per_pending_1);
  }

  // Bit 9 in IRQ_BASIC_PENDING indicates interrupts in Pending2 (interrupts 63-32):
  if (irq_basic_pending & (1UL << 9))
  {
    uint32_t per_pending_1 = getreg32(BCM2837B0_IRQ_PENDING_1_REG);
    irq = BCM_BASIC_IRQ_TOTAL + 32 + 31 - __builtin_clz(per_pending_1);
  }

  // Bits 20 ~ 10 in IRQ_BASIC_PENDING indicate GPU IRQs:
  if (irq_basic_pending & 0x1FFC00)
  {
    /* GPU IRQs */
    irq = 31 - __builtin_clz(irq_basic_pending);
  }
  else
  {

  }
#endif
  return fiq;
}

/****************************************************************************
 * Name: arm_decodefiq
 *
 * Description:
 *   This function is called from the FIQ vector handler in arm_vectors.S.
 *   At this point, the interrupt has been taken and the registers have
 *   been saved on the stack.  This function simply needs to determine the
 *   the fiq number of the interrupt and then to call arm_doirq to dispatch
 *   the interrupt.
 *
 *  Input Parameters:
 *   regs - A pointer to the register save area on the stack.
 *
 ****************************************************************************/

uint64_t arm_decodefiq(uint64_t *regs)
{
  int irq;

  up_lowputc('F');
  up_lowputc('I');
  up_lowputc('Q');
  return regs;
}

