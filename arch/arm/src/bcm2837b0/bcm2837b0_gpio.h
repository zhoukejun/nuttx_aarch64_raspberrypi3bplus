/************************************************************************************
 * arch/arm/src/bcm2837b0/bcm2837b0_gpio.h
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_GPIO_H
#define __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_GPIO_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
#define GPIOFSEL1_FSEL14_MASK	(0b111 << 12)
#define GPIOFSEL1_FSEL15_MASK	(0b111 << 15)

#define GPIOFSEL1_FSEL14_ALT5	(0b010 << 12)
#define GPIOFSEL1_FSEL15_ALT5	(0b010 << 15)

/* GPPUD register  */
#define GPPUD_OFF		(0b00 << 0)	/* OO = Off - disable pull-up/down */
#define GPPUD_ENABLE_PULL_DOWN	(0b01 << 0)	/* O1 = Enable pull-down control */
#define GPPUD_ENABLE_PULL_UP	(0b10 << 0)	/* 1O = Enable pull-up control */
#define GPPUD_RESERVED		(0b11 << 0)	/* 11 = Off - disable pull-up/down */

/* GPIO Pull-up/down Clock Register 0 */
#define GPPUDCLK0_BIT_14	(1 << 14)	/* Assert Clock on line 14 */
#define GPPUDCLK0_BIT_15	(1 << 15)	/* Assert Clock on line 15 */


/************************************************************************************
 * Public Types
 ************************************************************************************/

/************************************************************************************
 * Public Data
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

#endif // __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_MAP_H
