/****************************************************************************
 * arch/arm/src/bcm2837b0/bcm2837b0_lowputc.c
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <stdint.h>

#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"

#include "bcm2837b0_uart.h"
#include "bcm2837b0_gpio.h"
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Configuration **********************************************************/

/* Is there a UART enabled? */

#if defined(CONFIG_BCM2837B0_MINI_UART)
#  define HAVE_UART 1

/* Is there a serial console? */

#  if defined(CONFIG_MINI_UART_SERIAL_CONSOLE)
#    define HAVE_CONSOLE
#  else
#    undef HAVE_CONSOLE
#  endif

#else
#  undef HAVE_UART
#  undef HAVE_CONSOLE
#endif

#if defined(CONFIG_MINI_UART_SERIAL_CONSOLE)
#  define BCM2837B0_UART_BASE     BCM2837B0_MINI_UART_BASE
#  define BCM2837B0_UART_BAUD     CONFIG_MINI_UART_BAUD
#  define BCM2837B0_UART_BITS     CONFIG_MINI_UART_BITS
#  define BCM2837B0_UART_PARITY   CONFIG_MINI_UART_PARITY
#  define BCM2837B0_UART_2STOP    CONFIG_MINI_UART_2STOP
#else
#  error "No CONFIG_MINI_UART_SERIAL_CONSOLE Setting"
#endif


/* Calculate BAUD rate from PCLK1:
 *         baudrate = sysclk / (8*(baudrate_reg + 1))
 * Example:  115200 = 250 MHz / (8*(baudrate_reg + 1))
 * baudrate_reg = 270
 *
 */
#define UART_BAUD_REG (((BCM2837B0_SYSCLK / CONFIG_MINI_UART_BAUD) / 8) - 1)

/* Get mode setting */

#if BCM2837B0_UART_BITS == 7
#  if BCM2837B0_UART_PARITY == 0
#    error "7-bits, no parity mode not supported"
#  else
#    define BCM2837B0_UARTCR_MODE BCM2837B0_UARTCR_MODE7BITP
#  endif
#elif BCM2837B0_UART_BITS == 8
#  if BCM2837B0_UART_PARITY == 0
#    define BCM2837B0_UARTCR_MODE BCM2837B0_UARTCR_MODE8BIT
#  else
#    define BCM2837B0_UARTCR_MODE BCM2837B0_UARTCR_MODE8BITP
#  endif
#elif BCM2837B0_UART_BITS == 9
#  if BCM2837B0_UART_PARITY == 0
#    define BCM2837B0_UARTCR_MODE BCM2837B0_UARTCR_MODE9BIT
#  else
#    error "9-bits with parity not supported"
#  endif
#else
#  error "Number of bits not supported"
#endif

#if BCM2837B0_UART_PARITY == 0 || BCM2837B0_UART_PARITY == 2
#  define BCM2837B0_UARTCR_PARITY (0)
#elif BCM2837B0_UART_PARITY == 1
#  define BCM2837B0_UARTCR_PARITY BCM2837B0_UARTCR_PARITYODD
#else
#  error "Invalid parity selection"
#endif


#define BCM2837B0_UARTCR_VALUE \
  (BCM2837B0_UARTCR_MODE | BCM2837B0_UARTCR_PARITY   | BCM2837B0_UARTCR_STOP | \
   BCM2837B0_UARTCR_RUN  | BCM2837B0_UARTCR_RXENABLE | BCM2837B0_UARTCR_FIFOENABLE)

/****************************************************************************
 * Private Types
 ****************************************************************************/

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Private Data
 ****************************************************************************/

/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: up_lowputhex64
 *
 * Description:
 *   Output 64bits in hex on the serial console
 *
 ****************************************************************************/

void up_lowputhex64(uint64_t hex64)
{
  int i;
  char j;

  up_lowputc('0');
  up_lowputc('x');

  for (i = 64 - 4; i >= 0; i -= 4)
  {
    j = (hex64 >> i) & 0xF;
    if (j > 9)
      j = j - 10 + 'A';
    else
      j += '0';

    up_lowputc(j);
  }

  up_lowputc(0x0D);
  up_lowputc(0x0A);
}
/****************************************************************************
 * Name: up_lowputhex32
 *
 * Description:
 *   Output 32bits in hex on the serial console
 *
 ****************************************************************************/

void up_lowputhex32(uint32_t hex32)
{
  int i;
  unsigned char j;

  up_lowputc('0');
  up_lowputc('x');
  for (i = 32 - 4; i >= 0; i -= 4)
  {
    j = (hex32 >> i) & 0xF;
    if (j > 9)
      j = j - 10 + 'A';
    else
      j += '0';

    up_lowputc(j);
  }

  up_lowputc(0x0D);
  up_lowputc(0x0A);
}

/****************************************************************************
 * Name: up_lowputc
 *
 * Description:
 *   Output one byte on the serial console
 *
 ****************************************************************************/

void up_lowputc(char ch)
{
#ifdef HAVE_CONSOLE
  /* Wait until the TX FIFO is not full */

  while(1)
  {
    if(getreg32(BCM2837B0_AUX_MU_LSR_REG) & AUX_MU_LSR_TRANSMITTER_EMPTY)
      break;
  }

  /* Then send the character */
  putreg32((uint32_t)ch, BCM2837B0_AUX_MU_IO_REG);

#endif
}

/****************************************************************************
 * Name: up_lowsetup
 *
 * Description:
 *   This performs basic initialization of the UART used for the serial
 *   console.  Its purpose is to get the console output availabe as soon
 *   as possible.
 *
 ****************************************************************************/

void up_lowsetup(void)
{
#if defined(HAVE_CONSOLE) && !defined(CONFIG_SUPPRESS_UART_CONFIG)

  putreg32(getreg32(BCM2837B0_AUX_ENABLES_REG) | AUXENB_MINI_UART_ENABLE, BCM2837B0_AUX_ENABLES_REG);
  putreg32(AUX_MU_LCR_8_BIT_MODE, BCM2837B0_AUX_MU_LCR_REG);

  /* Calculate BAUD rate from PCLK1:
   *         baudrate = sysclk / (8*(baudrate_reg + 1))
   * Example:  115200 = 250 MHz / (8*(baudrate_reg + 1))
   * baudrate_reg = 270
   *
   */
  //putreg32(270, BCM2837B0_AUX_MU_BAUD_REG);
  putreg32(UART_BAUD_REG, BCM2837B0_AUX_MU_BAUD_REG);

#endif

  /* Configure GPIO0 pins to enable all UARTs in the configuration
   * (the serial driver later depends on this configuration)
   */

#ifdef HAVE_UART
  /*Clear GPIO14 GPIO15 Function select */
  putreg32(getreg32(BCM2837B0_GPFSEL1_REG) & (~GPIOFSEL1_FSEL14_MASK | ~GPIOFSEL1_FSEL15_MASK), BCM2837B0_GPFSEL1_REG); 

  /*Set GPIO14 GPIO15 Function to ALT5 Mini UART */
  putreg32(getreg32(BCM2837B0_GPFSEL1_REG) | (GPIOFSEL1_FSEL14_ALT5 | GPIOFSEL1_FSEL15_ALT5), BCM2837B0_GPFSEL1_REG); 

  putreg32(GPPUD_OFF, BCM2837B0_GPPUD_REG);
  for (int i = 0; i < 150; i++)
    asm volatile ("nop");

  putreg32((GPPUDCLK0_BIT_14 | GPPUDCLK0_BIT_15), BCM2837B0_GPPUDCLK0_REG);

  for (int i = 0; i < 150; i++)
    asm volatile ("nop");

  putreg32(GPPUD_OFF, BCM2837B0_GPPUD_REG);
  putreg32(GPPUD_OFF, BCM2837B0_GPPUDCLK0_REG);
#endif

  putreg32((AUX_MU_CNTL_TRANSMITTER_ENABLE | AUX_MU_CNTL_RECEIVER_ENABLE), BCM2837B0_AUX_MU_CNTL_REG);
}


