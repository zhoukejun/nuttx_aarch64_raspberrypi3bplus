/************************************************************************************
 * arch/arm/src/bcm2837b0/bcm2837b0_map.h
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_MAP_H
#define __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_MAP_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/

/* Memory Map ***********************************************************************/

#define BCM2837B0_PBASE			(0x3F000000) /* Physical Address for BCM2837B0 */

#define BCM2837B0_GPFSEL1_REG		(BCM2837B0_PBASE+0x00200004)
#define BCM2837B0_GPPUD_REG		(BCM2837B0_PBASE+0x00200094)
#define BCM2837B0_GPPUDCLK0_REG		(BCM2837B0_PBASE+0x00200098)

#define BCM2837B0_AUX_IRQ_PENDING_REG	(BCM2837B0_PBASE+0x00215000)
#define BCM2837B0_AUX_ENABLES_REG	(BCM2837B0_PBASE+0x00215004)
#define BCM2837B0_AUX_MU_IO_REG		(BCM2837B0_PBASE+0x00215040)
#define BCM2837B0_AUX_MU_IER_REG	(BCM2837B0_PBASE+0x00215044)
#define BCM2837B0_AUX_MU_IIR_REG	(BCM2837B0_PBASE+0x00215048)
#define BCM2837B0_AUX_MU_LCR_REG	(BCM2837B0_PBASE+0x0021504C)
#define BCM2837B0_AUX_MU_MCR_REG	(BCM2837B0_PBASE+0x00215050)
#define BCM2837B0_AUX_MU_LSR_REG	(BCM2837B0_PBASE+0x00215054)
#define BCM2837B0_AUX_MU_MSR_REG	(BCM2837B0_PBASE+0x00215058)
#define BCM2837B0_AUX_MU_SCRATCH_REG	(BCM2837B0_PBASE+0x0021505C)
#define BCM2837B0_AUX_MU_CNTL_REG	(BCM2837B0_PBASE+0x00215060)
#define BCM2837B0_AUX_MU_STAT_REG	(BCM2837B0_PBASE+0x00215064)
#define BCM2837B0_AUX_MU_BAUD_REG	(BCM2837B0_PBASE+0x00215068)

#define BCM2837B0_INTC_BASE		(BCM2837B0_PBASE + 0x0000B000) /* Interrupt Control Regs */

#define BCM2837B0_IRQ_BASIC_PENDING_REG	(BCM2837B0_INTC_BASE + 0x00000200)
#define BCM2837B0_IRQ_PENDING_1_REG	(BCM2837B0_INTC_BASE + 0x00000204)
#define BCM2837B0_IRQ_PENDING_2_REG	(BCM2837B0_INTC_BASE + 0x00000208)
#define BCM2837B0_FIQ_CONTROL_REG	(BCM2837B0_INTC_BASE + 0x0000020C)
#define BCM2837B0_ENABLE_IRQ_1_REG	(BCM2837B0_INTC_BASE + 0x00000210)
#define BCM2837B0_ENABLE_IRQ_2_REG	(BCM2837B0_INTC_BASE + 0x00000214)
#define BCM2837B0_ENABLE_BASIC_IRQ_REG	(BCM2837B0_INTC_BASE + 0x00000218)
#define BCM2837B0_DISABLE_IRQ_1_REG	(BCM2837B0_INTC_BASE + 0x0000021C)
#define BCM2837B0_DISABLE_IRQ_2_REG	(BCM2837B0_INTC_BASE + 0x00000220)
#define BCM2837B0_DISABLE_BASIC_IRQ_REG	(BCM2837B0_INTC_BASE + 0x00000224)


#define BCM2837B0_SYS_TIME_BASE		(BCM2837B0_PBASE + 0x00003000) /* System Timer Regs */

#define BCM2837B0_SYS_TIMER_CS_REG	(BCM2837B0_SYS_TIME_BASE + 0x00000000)
#define BCM2837B0_SYS_TIMER_CLO_REG	(BCM2837B0_SYS_TIME_BASE + 0x00000004)
#define BCM2837B0_SYS_TIMER_CHI_REG	(BCM2837B0_SYS_TIME_BASE + 0x00000008)
#define BCM2837B0_SYS_TIMER_C_0_REG	(BCM2837B0_SYS_TIME_BASE + 0x0000000C)
#define BCM2837B0_SYS_TIMER_C_1_REG	(BCM2837B0_SYS_TIME_BASE + 0x00000010)
#define BCM2837B0_SYS_TIMER_C_2_REG	(BCM2837B0_SYS_TIME_BASE + 0x00000014)
#define BCM2837B0_SYS_TIMER_C_3_REG	(BCM2837B0_SYS_TIME_BASE + 0x00000018)


#define BCM2837B0_ARM_TIME_BASE		(BCM2837B0_PBASE + 0x0000B000) /* ARM Timer Regs */

#define BCM2837B0_ARM_TIMER_LOAD_REG		(BCM2837B0_ARM_TIME_BASE + 0x00000400)
#define BCM2837B0_ARM_TIMER_VALUE_REG		(BCM2837B0_ARM_TIME_BASE + 0x00000404)
#define BCM2837B0_ARM_TIMER_CTRL_REG		(BCM2837B0_ARM_TIME_BASE + 0x00000408)
#define BCM2837B0_ARM_TIMER_CLEAR_ACK_REG	(BCM2837B0_ARM_TIME_BASE + 0x0000040C)
#define BCM2837B0_ARM_TIMER_RAW_IRQ_REG		(BCM2837B0_ARM_TIME_BASE + 0x00000410)
#define BCM2837B0_ARM_TIMER_MASKED_IRQ_REG	(BCM2837B0_ARM_TIME_BASE + 0x00000414)
#define BCM2837B0_ARM_TIMER_RELOAD_REG		(BCM2837B0_ARM_TIME_BASE + 0x00000418)
#define BCM2837B0_ARM_TIMER_PRE_DIV_REG		(BCM2837B0_ARM_TIME_BASE + 0x0000041C)
#define BCM2837B0_ARM_TIMER_FREE_R_CNT_REG	(BCM2837B0_ARM_TIME_BASE + 0x00000420)


/************************************************************************************
 * Public Types
 ************************************************************************************/

/************************************************************************************
 * Public Data
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

#endif // __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_MAP_H
