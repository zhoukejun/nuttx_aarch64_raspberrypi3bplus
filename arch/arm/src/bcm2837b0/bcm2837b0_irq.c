/****************************************************************************
 * arch/arm/src/bcm2837b0/bcm2837b0_irq.c
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <assert.h>

#include <nuttx/arch.h>

#include "up_internal.h"
#include "sctlr.h"
#include "up_arch.h"
#include "bcm2837b0_irq.h"
#include "bcm2837b0_map.h"

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Size of the interrupt stack allocation */

#define INTSTACK_ALLOC (CONFIG_SMP_NCPUS * INTSTACK_SIZE)

/****************************************************************************
 * Public Data
 ****************************************************************************/

/* g_current_regs[] holds a references to the current interrupt level
 * register storage structure.  If is non-NULL only during interrupt
 * processing.  Access to g_current_regs[] must be through the macro
 * CURRENT_REGS for portability.
 */

#ifdef CONFIG_SMP
/* For the case of configurations with multiple CPUs, then there must be one
 * such value for each processor that can receive an interrupt.
 */

volatile uint32_t *g_current_regs[CONFIG_SMP_NCPUS];
#else
volatile uint32_t *g_current_regs[1];
#endif

#if defined(CONFIG_SMP) && CONFIG_ARCH_INTERRUPTSTACK > 7
/* In the SMP configuration, we will need custom IRQ and FIQ stacks.
 * These definitions provide the aligned stack allocations.
 */

uint64_t g_irqstack_alloc[INTSTACK_ALLOC >> 3];
uint64_t g_fiqstack_alloc[INTSTACK_ALLOC >> 3];

/* These are arrays that point to the top of each interrupt stack */

uintptr_t g_irqstack_top[CONFIG_SMP_NCPUS] =
{
  (uintptr_t)g_irqstack_alloc + INTSTACK_SIZE,
#if CONFIG_SMP_NCPUS > 1
  (uintptr_t)g_irqstack_alloc + 2 * INTSTACK_SIZE,
#endif
#if CONFIG_SMP_NCPUS > 2
  (uintptr_t)g_irqstack_alloc + 3 * INTSTACK_SIZE,
#endif
#if CONFIG_SMP_NCPUS > 3
  (uintptr_t)g_irqstack_alloc + 4 * INTSTACK_SIZE
#endif
};

uintptr_t g_fiqstack_top[CONFIG_SMP_NCPUS] =
{
  (uintptr_t)g_fiqstack_alloc + INTSTACK_SIZE,
#if CONFIG_SMP_NCPUS > 1
  (uintptr_t)g_fiqstack_alloc + 2 * INTSTACK_SIZE,
#endif
#if CONFIG_SMP_NCPUS > 2
  (uintptr_t)g_fiqstack_alloc + 3 * INTSTACK_SIZE,
#endif
#if CONFIG_SMP_NCPUS > 3
  (uintptr_t)g_fiqstack_alloc + 4 * INTSTACK_SIZE
#endif
};

#endif

/* Symbols defined via the linker script */

extern uint64_t _vector_start; /* Beginning of vector block */
extern uint64_t _vector_end;   /* End+1 of vector block */


/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: up_irqinitialize
 *
 * Description:
 *   This function is called by up_initialize() during the bring-up of the
 *   system.  It is the responsibility of this function to but the interrupt
 *   subsystem into the working and ready state.
 *
 ****************************************************************************/

void up_irqinitialize(void)
{

  /* currents_regs is non-NULL only while processing an interrupt */

  CURRENT_REGS = NULL;

#ifndef CONFIG_SUPPRESS_INTERRUPTS

  /* And finally, enable interrupts */

  (void)up_irq_enable();
#endif
}

/****************************************************************************
 * Name: up_enable_irq
 *
 * Description:
 *   On many architectures, there are three levels of interrupt enabling: (1)
 *   at the global level, (2) at the level of the interrupt controller,
 *   and (3) at the device level.  In order to receive interrupts, they
 *   must be enabled at all three levels.
 *
 *   This function implements enabling of the device specified by 'irq'
 *   at the interrupt controller level if supported by the architecture
 *   (up_irq_restore() supports the global level, the device level is hardware
 *   specific).
 *
 *   Since this API is not supported on all architectures, it should be
 *   avoided in common implementations where possible.
 *
 ****************************************************************************/

void up_enable_irq(int irq)
{
  if (irq < BCM_BASIC_IRQ_TOTAL)
  {
    putreg32((0x1 << irq), BCM2837B0_ENABLE_BASIC_IRQ_REG);
  }
  else if ((irq < NR_IRQS - 32) && (irq >= BCM_BASIC_IRQ_TOTAL))
  {
    putreg32((0x1 << irq), BCM2837B0_ENABLE_IRQ_1_REG);
  }
  else if ((irq < NR_IRQS) && (irq >= NR_IRQS - 32))
  {
    putreg32((0x1 << irq), BCM2837B0_ENABLE_IRQ_2_REG);
  }
  else
  {
  }
}

/****************************************************************************
 * Name: up_disable_irq
 *
 * Description:
 *   This function implements disabling of the device specified by 'irq'
 *   at the interrupt controller level if supported by the architecture
 *   (up_irq_save() supports the global level, the device level is hardware
 *   specific).
 *
 *   Since this API is not supported on all architectures, it should be
 *   avoided in common implementations where possible.
 *
 ****************************************************************************/

void up_disable_irq(int irq)
{
  if (irq < BCM_BASIC_IRQ_TOTAL)
  {
    putreg32((0x1 << irq), BCM2837B0_DISABLE_BASIC_IRQ_REG);
  }
  else if ((irq < (NR_IRQS - 32)) && (irq >= BCM_BASIC_IRQ_TOTAL))
  {
    putreg32((0x1 << (irq - BCM_BASIC_IRQ_TOTAL)), BCM2837B0_DISABLE_IRQ_1_REG);
  }
  else if ((irq < NR_IRQS) && (irq >= NR_IRQS - 32))
  {
    putreg32((0x1 << (irq - BCM_BASIC_IRQ_TOTAL - 32)), BCM2837B0_DISABLE_IRQ_2_REG);
  }
  else
  {
  }
}

/****************************************************************************
 * Name: bcm2837b0_get_irq_id
 *
 * Description:
 *   This function check the IRQ Pending registers to get the IRQ Id.
 *
 *  Input Parameters:
 *  void
 *
 ****************************************************************************/

int bcm2837b0_get_irq_id(void)
{
  int irq;

  uint32_t irq_basic_pending = getreg32(BCM2837B0_IRQ_BASIC_PENDING_REG);

  // Bits 7 ~ 0 in IRQBASIC_PENDING represent interrupts :
  if (irq_basic_pending & 0xFF)
  {
    irq = 31 - __builtin_clz(irq_basic_pending);
  }

  // Bit 8 in IRQ_BASSIC_PENDING indicates interrupts in Pending1 (interrupts 31-0):
  if (irq_basic_pending & (1UL << 8))
  {
    uint32_t per_pending_1 = getreg32(BCM2837B0_IRQ_PENDING_1_REG);
    irq = BCM_BASIC_IRQ_TOTAL + 31 - __builtin_clz(per_pending_1);
  }

  // Bit 9 in IRQ_BASIC_PENDING indicates interrupts in Pending2 (interrupts 63-32):
  if (irq_basic_pending & (1UL << 9))
  {
    uint32_t per_pending_1 = getreg32(BCM2837B0_IRQ_PENDING_1_REG);
    irq = BCM_BASIC_IRQ_TOTAL + 32 + 31 - __builtin_clz(per_pending_1);
  }

  // Bits 20 ~ 10 in IRQ_BASIC_PENDING indicate GPU IRQs:
  if (irq_basic_pending & 0x1FFC00)
  {
    /* GPU IRQs */
    irq = 31 - __builtin_clz(irq_basic_pending);
  }
  else
  {

  }

  return irq;
}

/****************************************************************************
 * Name: arm_decodeirq
 *
 * Description:
 *   This function is called from the IRQ vector handler in arm_vectors.S.
 *   At this point, the interrupt has been taken and the registers have
 *   been saved on the stack.  This function simply needs to determine the
 *   the irq number of the interrupt and then to call arm_doirq to dispatch
 *   the interrupt.
 *
 *  Input Parameters:
 *   regs - A pointer to the register save area on the stack.
 *
 ****************************************************************************/

uint64_t arm_decodeirq(uint64_t *regs)
{
  int irq;

  up_lowputc('I');
  up_lowputc('R');
  up_lowputc('Q');
  irq = bcm2837b0_get_irq_id();
  regs = arm_doirq(irq, regs);
  return regs;
}

