/************************************************************************************
 * arch/arm/src/bcm2837b0/bcm2837b0_uart.h
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_UART_H
#define __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_UART_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

#include <nuttx/config.h>

#include "chip.h"

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
//#define CONFIG_BCM2837B0_MINI_UART_BAUD	115200
//#define CONFIG_MINI_UART_RXBUFSIZE	8
//#define CONFIG_MINI_UART_TXBUFSIZE	8
//#define CONFIG_MINI_UART_BITS		8
//#define CONFIG_MINI_UART_PARITY		0
//#define CONFIG_MINI_UART_SERIAL_CONSOLE 1
#define BCM2837B0_MINI_UART_IRQ		 BCM_PERIPHERALS_IRQ_UART_INT
/* Register Offsets *****************************************************************/


/* Register Addresses ***************************************************************/


/* Register Bitfield Definitions ****************************************************/

/* AUXIRQ register  */
#define AUXIRQ_MINI_UART_PENDING	(1 << 0) /* Bit 0:  Mini UART has an interrupt pending */

/* AUXENB register  */
#define AUXENB_MINI_UART_ENABLE		(1 << 0) /* Bit 0:  Mini UART enable */

/* AUX_MU_IO register  */
#define AUX_MU_IO_LS_8_BITS_MASK	(0xFF) /* Bits 7:0 Mask. DLAB = 1, LS 8 bits Baudrate read/write. DLAB = 0, Transmit/Receive data write/read */

/* AUX_MU_IER register */
#define AUX_MU_IER_MS_8_BITS_MASK	(0xFF) /* Bits 7:0 Mask. DLAB = 1, MS 8 bits Baudrate read/write. */
#define AUX_MU_IER_ENABLE_TRANSMIT_IRQ	(1 << 1) /* DLAB = 0 If this bit is set, the interrupt line is asserted whenever the transmit FIFO is empty. */
#define AUX_MU_IER_ENABLE_RECEIVE_IRQ	(1 << 0) /* DLAB = 0 If this bit is set, the interrupt line is asserted whenever the receive FIFO holds at least 1 byte. */

/* AUX_MU_IIR register */
#define AUX_MU_IIR_READ_IRQ_ID_MASK	(0b11 << 2) /* On read this register shows the interrupt ID bit. */
						    /* 00:NO irq, 01:Transmit holding register empty, 10:Receiver holds valid byte, 11:<Not possible> */
#define AUX_MU_IIR_WRITE_CLEAR_RECEIVE_FIFO	(1 << 1) /* Writing with bit 1 set will clear the receive FIFO */
#define AUX_MU_IIR_WRITE_CLEAR_TRANSMIT_FIFO	(1 << 2) /* Writing with bit 1 set will clear the transmit FIFO */
#define AUX_MU_IIR_IRQ_PENDING		(1 << 0) /* This bit is clear whenever an interrupt is pending */

/* AUX_MU_LCR register */
#define AUX_MU_LCR_DLAB_ACC		(1 << 7) /* If set the firt to Mini UART register give access the Baudrate register. */
						 /* During operation this bit must be cleared */
#define AUX_MU_LCR_BREAK		(1 << 6) /* If set high the UART1_TX line is pulled low continuously. */
						 /* If held for at least 12 bits times that will indicate a break condition */
#define AUX_MU_LCR_7_BIT_MODE		(0b00 << 0) /* 00: the UART works in 7-bit mode */
#define AUX_MU_LCR_8_BIT_MODE		(0b11 << 0) /* 11: the UART works in 8-bit mode */

/* AUX_MU_MCR register */
#define AUX_MU_MCR_RTS			(1 << 1) /* If clear the UART1_RTS line is high. */ 
						 /* If set the UART1_RTS line is low. This bit is ignored if the RTS is used for auto-flow control. */

/* AUX_MU_LSR register */
#define AUX_MU_LSR_TRANSMITTER_IDLE	(1 << 6) /* This bit is set if the transmit FIFO is empty and the transmitter is idle */
#define AUX_MU_LSR_TRANSMITTER_EMPTY	(1 << 5) /* This bit is set if the transmit FIFO can accept at least one byte */
#define AUX_MU_LSR_RECEIVER_OVERRUN	(1 << 1) /* This bit is set if there was a receiver overrun */
#define AUX_MU_LSR_DATA_READY		(1 << 0) /* This bit is set if the receive FIFO holds at least 1 symbol */


/* AUX_MU_MSR register */
#define AUX_MU_MSR_CTS_STATUS		(1 << 5) /* This bit is the inverse of the UART1_CTS input. */
						 /* Thus: If set the UART1_CTS pin is low. If clear the UART1_CTS pin is high */

/* AUX_MU_SCRATCH register */
#define AUX_MU_SCRANTCH_MASK		(0xFF << 0) /* One whole byte extra on top of the 134217728 provided by the SDC */

/* AUX_MU_CNTL register */
#define AUX_MU_CNTL_CTS_ASSERT_LEVEL	(1 << 7) /* This bit allows one to invert the CTS auto flow operation polarity. */ 
						 /* If set the CTS auto flow assert level is low.  If clear the CTS auto flow assert level is high */
#define AUX_MU_CNTL_RTS_ASSERT_LEVEL	(1 << 6) /* This bit allows one to invert the RTS auto flow operation polarity. */ 
						 /* If set the RTS auto flow assert level is low. If clear the RTS auto flow assert level is high */
#define AUX_MU_CNTL_RTS_AUTO_FLOW_LEVEL	(0b11 << 4) /* These two bits specify at what receiver FIFO level the RTS line is de-asserted in auto-flow mode.*/
						 /* 00 : De-assert RTS when the receive FIFO has 3 empty spaces left. */
						 /* 01 : De-assert RTS when the receive FIFO has 2 empty spaces left. */
						 /* 10 : De-assert RTS when the receive FIFO has 1 empty space left. */
						 /* 11 : De-assert RTS when the receive FIFO has 4 empty spaces left. */
#define AUX_MU_CNTL_ENABLE_CTS 		(1 << 3) /* If this bit is set the transmitter will stop if the CTS line is de-asserted. */
						 /* If this bit is clear the transmitter will ignore the status of the CTS line */
#define AUX_MU_CNTL_ENABLE_RTS		(1 << 2) /* If this bit is set the RTS line will de-assert if the receive FIFO reaches it 'auto flow' level. */
						 /* In fact the RTS line will behave as an RTR (Ready To Receive) line. */
						 /* If this bit is clear the RTS line is controlled by the AUX_MU_MCR_REG register bit 1. */
#define AUX_MU_CNTL_TRANSMITTER_ENABLE	(1 << 1) /* If this bit is set the mini UART transmitter is enabled. */
						 /* If this bit is clear the mini UART transmitter is disabled */
#define AUX_MU_CNTL_RECEIVER_ENABLE	(1 << 0) /* If this bit is set the mini UART receiver is enabled. */
						 /* If this bit is clear the mini UART receiver is disabled */


/* AUX_MU_STAT register */
#define AUX_MU_STAT_TRANSMIT_FIFO_FILL_LEVEL	(0b111 << 24)	/* Bit 27:24 These bits shows how many symbols are stored in the transmit FIFO */
								/* The value is in the range 0-8 */
#define AUX_MU_STAT_RECEIVE_FIFO_FILL_LEVEL	(0b111 << 16)	/* Bit 19:16 These bits shows how many symbols are stored in the receive FIFO */
								/* The value is in the range 0-8 */
#define AUX_MU_STAT_TRANSMITTER_DONE		(1 << 9)	/* Bit 9 This bit is set if the transmitter is idle and the transmit FIFO is empty. */
								/* It is a logic AND of bits 3 and 8 */
#define AUX_MU_STAT_TRANSMIT_FIFO_EMPTY		(1 << 8)	/* Bit 8 If this bit is set the transmitter FIFO is empty. */
								/* Thus it FIFO is empty can accept 8 symbols. */
#define AUX_MU_STAT_CTS_LINE_STATUS			(1 << 7)	/* Bit 7 This bit shows the status of the UART1_CTS line. */
#define AUX_MU_STAT_RTS_LINE_STATUS			(1 << 6)	/* Bit 6 This bit shows the status of the UART1_RTS line. */
#define AUX_MU_STAT_TRANSMIT_FIFO_FULL		(1 << 5)	/* Bit 5 This is the inverse of bit 1 */
#define AUX_MU_STAT_RECEIVER_OVERRUN			(1 << 4)	/* This bit is set if there was a receiver overrun. */ 
								/* That is one or more characters arrived whilst the receive FIFO was full. */
								/* The newly arrived characters have been discarded. */
								/* This bit is cleared each time the AUX_MU_LSR_REG register is read. */
#define AUX_MU_STAT_TRANSMITTER_IDLE			(1 << 3)	/* If this bit is set the transmitter is idle. */
								/* If this bit is clear the transmitter is idle. */
#define AUX_MU_STAT_RECEIVER_IDLE			(1 << 2)	/* If this bit is set the receiver is idle. */
								/* If this bit is clear the receiver is busy. */
								/* This bit can change unless the receiver is disabled */
#define AUX_MU_STAT_SPACE_AVAILABLE			(1 << 1)	/* If this bit is set the mini UART transmitter FIFO can accept at least one more symbol. */
								/* If this bit is clear the mini UART transmitter FIFO is full */
#define AUX_MU_STAT_SYMBOL_AVAILABLE			(1 << 0)	/* If this bit is set the mini UART receive FIFO contains at least 1 symbol */
								/* If this bit is clear the mini UART receiver FIFO is empty */

/* AUX_MU_BAUD register */
#define AUX_MU_BAUD_MASK		(0xFFFF < 0) /* Bit 15:0 Mini UART baudrate counter */


/************************************************************************************
 * Public Types
 ************************************************************************************/

/************************************************************************************
 * Public Data
 ************************************************************************************/

/************************************************************************************
 * Public Functions
 ************************************************************************************/

#endif /* __ARCH_ARM_SRC_BCM2837B0_BCM2837B0_UART_H */
