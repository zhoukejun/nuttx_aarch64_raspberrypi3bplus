############################################################################
# arch/arm/bcm2837b0/Make.defs
#
#   Copyright 2019 NuttX. All rights reserved.
#   Author: Kejun ZHOU <zhoukejun@outlook.com>
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in
#    the documentation and/or other materials provided with the
#    distribution.
# 3. Neither the name Gregory Nutt nor the names of its contributors may be
#    used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
############################################################################

# The vector table is the "head" object, i.e., the one that must forced into
# the link in order to draw in all of the other components

HEAD_ASRC  = arm_vectortab.S

ifeq ($(CONFIG_BUILD_KERNEL),y)
STARTUP_OBJS = crt0$(OBJEXT)
endif

# Force the start-up logic to be at the beginning of the .text to simplify
# debug.

ifeq ($(CONFIG_PAGING),y)
CMN_ASRCS  = arm_pghead.S
else
CMN_ASRCS  = arm_head.S
ifeq ($(CONFIG_SMP),y)
CMN_ASRCS += arm_cpuhead.S
endif
endif

# Common assembly language files

CMN_ASRCS += arm_vectors.S arm_fpuconfig.S arm_fullcontextrestore.S
CMN_ASRCS += arm_saveusercontext.S arm_vectoraddrexcptn.S arm_vfork.S
CMN_ASRCS += arm_testset.S arm_fetchadd.S

# Common C source files

CMN_CSRCS  = up_initialize.c up_interruptcontext.c up_exit.c up_createstack.c
CMN_CSRCS += up_releasestack.c up_usestack.c up_vfork.c up_puts.c up_mdelay.c
CMN_CSRCS += up_stackframe.c up_udelay.c up_modifyreg32.c up_modifyreg64.c

CMN_CSRCS += arm_assert.c arm_blocktask.c arm_copyfullstate.c
CMN_CSRCS += arm_doirq.c arm_initialstate.c #arm_mmu.c
CMN_CSRCS += arm_releasepending.c arm_reprioritizertr.c
CMN_CSRCS += arm_schedulesigaction.c arm_sigdeliver.c arm_syscall.c
CMN_CSRCS += arm_unblocktask.c

ifneq ($(CONFIG_ARCH_IDLE_CUSTOM),y)
CMN_CSRCS += up_idle.c
endif

ifeq ($(CONFIG_SMP),y)
CMN_CSRCS += arm_cpuindex.c arm_cpustart.c arm_cpupause.c arm_cpuidlestack.c
CMN_CSRCS += arm_scu.c
endif

# Use common heap allocation for now (may need to be customized later)

CMN_CSRCS += up_allocateheap.c

# Configuration dependent C and assembly language files

ifeq ($(CONFIG_PAGING),y)
CMN_CSRCS += arm_allocpage.c arm_checkmapping.c arm_pginitialize.c
CMN_CSRCS += arm_va2pte.c
endif

ifeq ($(CONFIG_BUILD_KERNEL),y)
CMN_CSRCS += up_task_start.c up_pthread_start.c arm_signal_dispatch.c
endif

ifeq ($(CONFIG_ARCH_ADDRENV),y)
CMN_CSRCS += arm_addrenv.c arm_addrenv_utils.c arm_pgalloc.c
ifeq ($(CONFIG_ARCH_STACK_DYNAMIC),y)
CMN_CSRCS += arm_addrenv_ustack.c
endif
ifeq ($(CONFIG_ARCH_KERNEL_STACK),y)
CMN_CSRCS += arm_addrenv_kstack.c
endif
ifeq ($(CONFIG_MM_SHM),y)
CMN_CSRCS += arm_addrenv_shm.c
endif
endif

ifeq ($(CONFIG_MM_PGALLOC),y)
CMN_CSRCS += arm_physpgaddr.c
ifeq ($(CONFIG_ARCH_PGPOOL_MAPPING),y)
CMN_CSRCS += arm_virtpgaddr.c
endif
endif

ifeq ($(CONFIG_ELF),y)
CMN_CSRCS += arm_coherent_dcache.c
else ifeq ($(CONFIG_MODULE),y)
CMN_CSRCS += arm_coherent_dcache.c
endif

ifeq ($(CONFIG_ARCH_FPU),y)
CMN_ASRCS += arm_savefpu.S arm_restorefpu.S
CMN_CSRCS += arm_copyarmstate.c
endif

ifeq ($(CONFIG_STACK_COLORATION),y)
CMN_CSRCS += up_checkstack.c
endif

# bcm2837b0-specific assembly language files

CHIP_ASRCS  =

# bcm2837b0-specific C source files

#CHIP_CSRCS  = bcm2837b0_boot.c bcm2837b0_memorymap.c bcm2837b0_clockconfig.c bcm2837b0_irq.c
#CHIP_CSRCS += bcm2837b0_timerisr.c bcm2837b0_gpio.c bcm2837b0_iomuxc.c
CHIP_CSRCS += bcm2837b0_boot.c bcm2837b0_irq.c bcm2837b0_fiq.c bcm2837b0_synchronous.c bcm2837b0_serror.c bcm2837b0_armtimer_isr.c bcm2837b0_serial.c bcm2837b0_lowputc.c

ifeq ($(CONFIG_SMP),y)
CHIP_CSRCS += bcm2837b0_cpuboot.c
endif
