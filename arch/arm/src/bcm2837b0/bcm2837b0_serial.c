/****************************************************************************
 * arch/arm/src/bcm2837b0/bcm2837b0_serial.c
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>

#include <sys/types.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <semaphore.h>
#include <string.h>
#include <errno.h>
#include <debug.h>
#include <nuttx/irq.h>
#include <nuttx/arch.h>
#include <nuttx/fs/ioctl.h>
#include <nuttx/serial/serial.h>

#include "chip.h"
#include "up_arch.h"
#include "up_internal.h"
#include "bcm2837b0_uart.h"
#include "bcm2837b0.h"


/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* Some sanity checks *******************************************************/

/* Is there a UART enabled? */

#if !defined(CONFIG_BCM2837B0_MINI_UART)
#  error "No UARTs enabled"
#endif

/* Is there a serial console? */

#if defined(CONFIG_MINI_UART_SERIAL_CONSOLE)
#  define HAVE_CONSOLE 1
#else
#  undef HAVE_CONSOLE
#endif

/* If we are not using the serial driver for the console, then we
 * still must provide some minimal implementation of up_putc().
 */

#ifdef USE_SERIALDRIVER

/* Whether Mini UART will be tty0/console? */

#if defined(CONFIG_MINI_UART_SERIAL_CONSOLE) || !defined(HAVE_CONSOLE)
#  ifdef HAVE_CONSOLE
#    ifndef CONFIG_BCM2837B0_MINI_UART
#      error "Mini UART not selected, cannot be console"
#    endif
#    define CONSOLE_DEV   g_mini_uart_port     /* MINI UART is console */
#  endif
#  define TTYS0_DEV       g_mini_uart_port     /* MINI UART is tty0 */
#else
#  warning "No CONFIG_UARTn_SERIAL_CONSOLE Setting"
#endif


/****************************************************************************
 * Private Types
 ****************************************************************************/

struct up_dev_s
{
  uint32_t baud;      /* Configured baud */
  uint16_t ier;       /* Saved IER value */
  uint16_t sr;        /* Saved SR value (only used during interrupt processing) */
  uint8_t  irq;       /* IRQ associated with this UART */
  uint8_t  parity;    /* 0=none, 1=odd, 2=even */
  uint8_t  bits;      /* Number of bits (7 or 8) */
  bool     stopbits2; /* true: Configure with 2 stop bits instead of 1 */
};

/****************************************************************************
 * Private Function Prototypes
 ****************************************************************************/

/* Internal Helpers */

static inline uint32_t up_serialin(uint32_t reg_addr);
static inline void up_serialout(uint32_t reg_addr, uint32_t value);
static inline void up_disableuartint(struct up_dev_s *priv, uint16_t *ier);
static inline void up_restoreuartint(struct up_dev_s *priv, uint16_t ier);
#ifdef HAVE_CONSOLE
static inline void up_waittxnotfull(struct up_dev_s *priv);
#endif

/* Serial Driver Methods */

static int  up_setup(struct uart_dev_s *dev);
static void up_shutdown(struct uart_dev_s *dev);
static int  up_attach(struct uart_dev_s *dev);
static void up_detach(struct uart_dev_s *dev);
static int  up_interrupt(int irq, void *context, void *arg);
static int  up_ioctl(struct file *filep, int cmd, unsigned long arg);
static int  up_receive(struct uart_dev_s *dev, uint32_t *status);
static void up_rxint(struct uart_dev_s *dev, bool enable);
static bool up_rxavailable(struct uart_dev_s *dev);
static void up_send(struct uart_dev_s *dev, int ch);
static void up_txint(struct uart_dev_s *dev, bool enable);
static bool up_txready(struct uart_dev_s *dev);
static bool up_txempty(struct uart_dev_s *dev);

/****************************************************************************
 * Private Data
 ****************************************************************************/

static const struct uart_ops_s g_uart_ops =
{
  .setup          = up_setup,
  .shutdown       = up_shutdown,
  .attach         = up_attach,
  .detach         = up_detach,
  .ioctl          = up_ioctl,
  .receive        = up_receive,
  .rxint          = up_rxint,
  .rxavailable    = up_rxavailable,
#ifdef CONFIG_SERIAL_IFLOWCONTROL
  .rxflowcontrol  = NULL,
#endif
  .send           = up_send,
  .txint          = up_txint,
  .txready        = up_txready,
  .txempty        = up_txempty,
};

/* I/O buffers */

#ifdef CONFIG_BCM2837B0_MINI_UART
static char g_mini_uart_rxbuffer[CONFIG_MINI_UART_RXBUFSIZE];
static char g_mini_uart_txbuffer[CONFIG_MINI_UART_TXBUFSIZE];
#endif

/* This describes the state of the BCM2837B0 mini uart port. */

#ifdef CONFIG_BCM2837B0_MINI_UART
static struct up_dev_s g_mini_uart_priv =
{
  .baud           = CONFIG_MINI_UART_BAUD,
  .irq            = BCM2837B0_MINI_UART_IRQ,
//  .parity         = CONFIG_MINI_UART_PARITY,
  .bits           = CONFIG_MINI_UART_BITS,
//  .stopbits1      = CONFIG_MINI_UART_1STOP,
};

static uart_dev_t g_mini_uart_port =
{
  .recv     =
  {
    .size   = CONFIG_MINI_UART_RXBUFSIZE,
    .buffer = g_mini_uart_rxbuffer,
  },
  .xmit     =
  {
    .size   = CONFIG_MINI_UART_TXBUFSIZE,
    .buffer = g_mini_uart_txbuffer,
  },
  .ops      = &g_uart_ops,
  .priv     = &g_mini_uart_priv,
};
#endif


/****************************************************************************
 * Private Functions
 ****************************************************************************/

/****************************************************************************
 * Name: up_serialin
 ****************************************************************************/

static inline uint32_t up_serialin(uint32_t reg_addr)
{
  return getreg32(reg_addr);
}

/****************************************************************************
 * Name: up_serialout
 ****************************************************************************/

static inline void up_serialout(uint32_t reg_addr, uint32_t value)
{
  putreg32(value, reg_addr);
}

/****************************************************************************
 * Name: up_disableuartint
 ****************************************************************************/

static inline void up_disableuartint(struct up_dev_s *priv, uint16_t *ier)
{
  if (ier)
    {
      *ier = priv->ier;
    }

  priv->ier = 0;
  up_serialout(BCM2837B0_AUX_MU_IER_REG, 0);
}

/****************************************************************************
 * Name: up_restoreuartint
 ****************************************************************************/

static inline void up_restoreuartint(struct up_dev_s *priv, uint16_t ier)
{
  priv->ier = ier;
  up_serialout(BCM2837B0_AUX_MU_IER_REG, ier);
}

/****************************************************************************
 * Name: up_waittxnotfull
 ****************************************************************************/

#ifdef HAVE_CONSOLE
static inline void up_waittxnotfull(struct up_dev_s *priv)
{
  int tmp;

  /* Limit how long we will wait for the TX available condition */

  for (tmp = 1000 ; tmp > 0 ; tmp--)
    {
      /* Check TX FIFO is full */

      if ((up_serialin(BCM2837B0_AUX_MU_STAT_REG) & AUX_MU_LSR_TRANSMITTER_EMPTY) != 0)
        {
          /* The TX FIFO is not full... return */
          break;
        }
    }
}
#endif

/****************************************************************************
 * Name: up_setup
 *
 * Description:
 *   Configure the UART baud, bits, parity, fifos, etc. This
 *   method is called the first time that the serial port is
 *   opened.
 *
 ****************************************************************************/

static int up_setup(struct uart_dev_s *dev)
{
#ifndef CONFIG_SUPPRESS_UART_CONFIG
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  uint32_t baud;
  uint32_t lcr;

  /* Set the BAUD rate */

  baud = ((BCM2837B0_SYSCLK / CONFIG_MINI_UART_BAUD) / 8) - 1;
  up_serialout(BCM2837B0_AUX_MU_BAUD_REG, baud);

  /* Get mode setting */

  if (priv->bits == 7)
    {
      DEBUGASSERT(priv->parity != 0);
      lcr |= AUX_MU_LCR_7_BIT_MODE;
    }
  else if (priv->bits == 8)
    {
      lcr |= AUX_MU_LCR_8_BIT_MODE;
    }
  else
    {
     
    }

  up_serialout(BCM2837B0_AUX_MU_LCR_REG, lcr);

  /* Clear FIFOs */

  up_serialout(BCM2837B0_AUX_MU_IIR_REG, AUX_MU_IIR_WRITE_CLEAR_TRANSMIT_FIFO | AUX_MU_IIR_WRITE_CLEAR_RECEIVE_FIFO);


  /* Set up the IER */

  priv->ier = up_serialin(BCM2837B0_AUX_MU_IER_REG);
#endif
  return OK;
}

/****************************************************************************
 * Name: up_shutdown
 *
 * Description:
 *   Disable the UART.  This method is called when the serial
 *   port is closed
 *
 ****************************************************************************/

static void up_shutdown(struct uart_dev_s *dev)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  up_disableuartint(priv, NULL);
}

/****************************************************************************
 * Name: up_attach
 *
 * Description:
 *   Configure the UART to operation in interrupt driven mode.  This method is
 *   called when the serial port is opened.  Normally, this is just after the
 *   the setup() method is called, however, the serial console may operate in
 *   a non-interrupt driven mode during the boot phase.
 *
 *   RX and TX interrupts are not enabled when by the attach method (unless the
 *   hardware supports multiple levels of interrupt enabling).  The RX and TX
 *   interrupts are not enabled until the txint() and rxint() methods are called.
 *
 ****************************************************************************/

static int up_attach(struct uart_dev_s *dev)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  int ret;

  /* Attach and enable the IRQ */

  ret = irq_attach(priv->irq, up_interrupt, dev);
  if (ret == OK)
    {
      /* Enable the interrupt (RX and TX interrupts are still disabled
       * in the UART
       */

      up_enable_irq(priv->irq);
    }

  return ret;
}

/****************************************************************************
 * Name: up_detach
 *
 * Description:
 *   Detach UART interrupts.  This method is called when the serial port is
 *   closed normally just before the shutdown method is called.  The exception is
 *   the serial console which is never shutdown.
 *
 ****************************************************************************/

static void up_detach(struct uart_dev_s *dev)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  up_disable_irq(priv->irq);
  irq_detach(priv->irq);
}

/****************************************************************************
 * Name: up_interrupt
 *
 * Description:
 *   This is the UART interrupt handler.  It will be invoked
 *   when an interrupt received on the 'irq'  It should call
 *   uart_transmitchars or uart_receivechar to perform the
 *   appropriate data transfers.  The interrupt handling logic\
 *   must be able to map the 'irq' number into the approprite
 *   uart_dev_s structure in order to call these functions.
 *
 ****************************************************************************/

static int up_interrupt(int irq, void *context, void *arg)
{
  struct uart_dev_s *dev = (struct uart_dev_s *)arg;
  struct up_dev_s   *priv;
  int                passes;
  bool               handled;

  DEBUGASSERT(dev != NULL && dev->priv != NULL);
  priv = (struct up_dev_s *)dev->priv;

  /* Loop until there are no characters to be transferred or,
   * until we have been looping for a long time.
   */

  handled = true;
  for (passes = 0; passes < 256 && handled; passes++)
    {
      handled = false;

      /* Get the current UART status  */

       priv->sr = up_serialin(BCM2837B0_AUX_MU_STAT_REG);

      /* Handle incoming, receive bytes (with or without timeout) */

      if ((priv->sr  & AUX_MU_STAT_RECEIVE_FIFO_FILL_LEVEL) != 0 && /* Data available in Rx FIFO */
          (priv->ier & AUX_MU_IER_ENABLE_RECEIVE_IRQ) != 0)   /* Rx FIFO interrupts enabled */
        {
          /* Rx buffer not empty ... process incoming bytes */

          uart_recvchars(dev);
          handled = true;
        }

      /* Handle outgoing, transmit bytes */

      if ((priv->sr & AUX_MU_STAT_TRANSMIT_FIFO_EMPTY) != 0 && /* Tx FIFO not full */
          (priv->ier & AUX_MU_IER_ENABLE_TRANSMIT_IRQ) != 0)   /* Tx FIFO empty interrupt enabled */
        {
          /* Tx FIFO not full ... process outgoing bytes */

          uart_xmitchars(dev);
          handled = true;
        }
    }

  return OK;
}

/****************************************************************************
 * Name: up_ioctl
 *
 * Description:
 *   All ioctl calls will be routed through this method
 *
 ****************************************************************************/

static int up_ioctl(struct file *filep, int cmd, unsigned long arg)
{
#ifdef CONFIG_SERIAL_TIOCSERGSTRUCT
  struct inode      *inode = filep->f_inode;
  struct uart_dev_s *dev   = inode->i_private;
#endif
  int                ret    = OK;

  switch (cmd)
    {
#ifdef CONFIG_SERIAL_TIOCSERGSTRUCT
    case TIOCSERGSTRUCT:
      {
         struct up_dev_s *user = (struct up_dev_s *)arg;
         if (!user)
           {
             ret = -EINVAL;
           }
         else
           {
             memcpy(user, dev, sizeof(struct up_dev_s));
           }
       }
       break;
#endif

    default:
      ret = -ENOTTY;
      break;
    }

  return ret;
}

/****************************************************************************
 * Name: up_receive
 *
 * Description:
 *   Called (usually) from the interrupt level to receive one
 *   character from the UART.  Error bits associated with the
 *   receipt are provided in the return 'status'.
 *
 ****************************************************************************/

static int up_receive(struct uart_dev_s *dev, uint32_t *status)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  uint32_t rxbufr;

  rxbufr  = up_serialin(BCM2837B0_AUX_MU_IO_REG) & AUX_MU_IO_LS_8_BITS_MASK;
  *status = (uint32_t)priv->sr << 16 | rxbufr;
  return rxbufr & 0xff;
}

/****************************************************************************
 * Name: up_rxint
 *
 * Description:
 *   Call to enable or disable RX interrupts
 *
 ****************************************************************************/

static void up_rxint(struct uart_dev_s *dev, bool enable)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  if (enable)
    {
      /* Receive an interrupt when the Rx FIFO is half full (or if a timeout
       * occurs while the Rx FIFO is not empty).
       */

#ifndef CONFIG_SUPPRESS_SERIAL_INTS
      priv->ier |= AUX_MU_IER_ENABLE_RECEIVE_IRQ;
#endif
    }
  else
    {
      priv->ier &= ~AUX_MU_IER_ENABLE_RECEIVE_IRQ;
    }
  up_serialout(BCM2837B0_AUX_MU_IER_REG, priv->ier);
}

/****************************************************************************
 * Name: up_rxavailable
 *
 * Description:
 *   Return true if the receive fifo is not empty
 *
 ****************************************************************************/

static bool up_rxavailable(struct uart_dev_s *dev)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  return ((up_serialin(BCM2837B0_AUX_MU_LCR_REG) & AUX_MU_LSR_DATA_READY) != 0);
}

/****************************************************************************
 * Name: up_send
 *
 * Description:
 *   This method will send one byte on the UART
 *
 ****************************************************************************/

static void up_send(struct uart_dev_s *dev, int ch)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  up_serialout(BCM2837B0_AUX_MU_IO_REG, (uint32_t)ch);
}

/****************************************************************************
 * Name: up_txint
 *
 * Description:
 *   Call to enable or disable TX interrupts
 *
 ****************************************************************************/

static void up_txint(struct uart_dev_s *dev, bool enable)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  if (enable)
    {
      /* Set to receive an interrupt when the TX fifo is half emptied */

#ifndef CONFIG_SUPPRESS_SERIAL_INTS
      priv->ier |= AUX_MU_IER_ENABLE_TRANSMIT_IRQ;
#endif
    }
  else
    {
      /* Disable the TX interrupt */

      priv->ier &= ~AUX_MU_IER_ENABLE_TRANSMIT_IRQ;
    }
  up_serialout(BCM2837B0_AUX_MU_IER_REG, priv->ier);
}

/****************************************************************************
 * Name: up_txready
 *
 * Description:
 *   Return true if the tranmsitter is idle and transmit fifo is empty
 *
 ****************************************************************************/

static bool up_txready(struct uart_dev_s *dev)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  return ((up_serialin(BCM2837B0_AUX_MU_STAT_REG) & AUX_MU_STAT_TRANSMITTER_DONE) != 0);
}

/****************************************************************************
 * Name: up_txempty
 *
 * Description:
 *   Return true if the transmit fifo is empty
 *
 ****************************************************************************/

static bool up_txempty(struct uart_dev_s *dev)
{
  struct up_dev_s *priv = (struct up_dev_s *)dev->priv;
  return ((up_serialin(BCM2837B0_AUX_MU_STAT_REG) & AUX_MU_STAT_TRANSMIT_FIFO_EMPTY) != 0);
}

/****************************************************************************
 * Public Functions
 ****************************************************************************/

/****************************************************************************
 * Name: up_earlyserialinit
 *
 * Description:
 *   Performs the low level UART initialization early in
 *   debug so that the serial console will be available
 *   during bootup.  This must be called before up_serialinit.
 *
 ****************************************************************************/
void up_earlyserialinit(void)
{
  /* NOTE:  All GPIO configuration for the UARTs was performed in
   * up_lowsetup
   */

  /* Disable all UARTS */

  up_disableuartint(TTYS0_DEV.priv, NULL);

  /* Configuration whichever one is the console */

#ifdef HAVE_CONSOLE
  CONSOLE_DEV.isconsole = true;
  up_setup(&CONSOLE_DEV);
#endif
}

/****************************************************************************
 * Name: up_serialinit
 *
 * Description:
 *   Register serial console and serial ports.  This assumes
 *   that up_earlyserialinit was called previously.
 *
 ****************************************************************************/
void up_serialinit(void)
{
  /* Register the console */

#ifdef HAVE_CONSOLE
  (void)uart_register("/dev/console", &CONSOLE_DEV);
#endif

  /* Register all UARTs */

  (void)uart_register("/dev/ttyS0", &TTYS0_DEV);
}
/****************************************************************************
 * Name: up_putc
 *
 * Description:
 *   Provide priority, low-level access to support OS debug  writes
 *
 ****************************************************************************/

int up_putc(int ch)
{
#ifdef HAVE_CONSOLE
  struct up_dev_s *priv = (struct up_dev_s *)CONSOLE_DEV.priv;
  uint16_t ier;

  up_disableuartint(priv, &ier);
  up_waittxnotfull(priv);
  up_serialout(BCM2837B0_AUX_MU_IO_REG, (uint32_t)ch);

  /* Check for LF */

  if (ch == '\n')
    {
      /* Add CR */

      up_waittxnotfull(priv);
      up_serialout(BCM2837B0_AUX_MU_IO_REG, (uint32_t)'\r');
    }

  up_waittxnotfull(priv);
  up_restoreuartint(priv, ier);
#endif
  return ch;
}

#else /* USE_SERIALDRIVER */

/****************************************************************************
 * Name: up_putc
 *
 * Description:
 *   Provide priority, low-level access to support OS debug writes
 *
 ****************************************************************************/

int up_putc(int ch)
{
#ifdef HAVE_CONSOLE
  /* Check for LF */

  if (ch == '\n')
    {
      /* Add CR */

      up_lowputc('\r');
    }

  up_lowputc(ch);
#endif
  return ch;
}

#endif /* USE_SERIALDRIVER */
