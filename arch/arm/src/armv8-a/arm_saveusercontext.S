/****************************************************************************
 * arch/arm/src/armv8-a/arm_saveusercontext.S
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/irq.h>
#include "up_internal.h"

	.file	"arm_saveusercontext.S"

/****************************************************************************
 * Public Symbols
 ****************************************************************************/

	.globl	up_saveusercontext

/****************************************************************************
 * Public Functions
 ****************************************************************************/

	.text

/****************************************************************************
 * Name: up_saveusercontext
 ****************************************************************************/

	.globl	up_saveusercontext
	.type	up_saveusercontext, function

up_saveusercontext:
#if 0 //ZK
	/* On entry, a1 (x0) holds address of struct xcptcontext */

	add	x1, x0, #(8*REG_X4)
	stmia	r1, {r4-r14}

	/* Save the current cpsr */

	mrs		r2, cpsr		/* R3 = CPSR value */
	add		r1, r0, #(4*REG_CPSR)
	str		r2, [r1]

	/* Save the return address as the PC so that we return to the exit from
	 * this function.
	 */

	add		x1, x0, #(8*REG_PC)
	str		lr, [x1]

	/* Save the floating point registers.
	 * REVISIT:  Not all of the floating point registers need to be saved.
	 * Some are volatile and need not be preserved across functions calls.
	 * But right now, I can't find the definitive list of the volatile
	 * floating point registers.
	 */
#endif //ZK
#ifdef CONFIG_ARCH_FPU
	add		x1, x0, #(8*REG_Q0)		/* R1=Address of FP register storage */

	/* Store all floating point registers.  Registers are stored in numeric order,
	 * q0, q1, ... in increasing address order.
	 */
	/* Save the full FP context */
        stp     q0,q1, [x1], #0x20
        stp     q2,q3, [x1], #0x20
        stp     q4,q5, [x1], #0x20
        stp     q6,q7, [x1], #0x20
        stp     q8,q9, [x1], #0x20
        stp     q10,q11, [x1], #0x20
        stp     q12,q13, [x1], #0x20
        stp     q14,q15, [x1], #0x20
        stp     q16,q17, [x1], #0x20
        stp     q18,q19, [x1], #0x20
        stp     q20,q21, [x1], #0x20
        stp     q22,q23, [x1], #0x20
        stp     q24,q25, [x1], #0x20
        stp     q26,q27, [x1], #0x20
        stp     q28,q29, [x1], #0x20
        stp     q30,q31, [x1], #0x20
        mrs     x2, fpcr		/* Fetch the FPCR */
        mrs     x3, fpsr		/* Fetch the FPSR */
        stp     x2, x3, [x1], #0x10

#endif

	/* Return 0 now indicating that this return is not a context switch */

	mov		x0, #0			/* Return value == 0 */
	.size	up_saveusercontext, . - up_saveusercontext
	.end
