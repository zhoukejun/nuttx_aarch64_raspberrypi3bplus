/************************************************************************************
 * arch/arm/src/armv8-a/sctlr.h
 * CP15 System Control Registers
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * References:
 *
 *  "Cortex-A53 MPCore, Technical Reference Manual", Revision: r0p1, Copyright © 2010
 *   ARM. All rights reserved. ARM DDI 0434B (ID101810)
 *  "ARM® Architecture Reference Manual, ARMv8-A edition", Copyright ©
 *   1996-1998, 2000, 2004-2012 ARM. All rights reserved. ARM DDI 0406C.b (ID072512)
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ************************************************************************************/

#ifndef __ARCH_ARM_SRC_ARMV8_A_SCTLR_H
#define __ARCH_ARM_SRC_ARMV8_A_SCTLR_H

/************************************************************************************
 * Included Files
 ************************************************************************************/

/************************************************************************************
 * Pre-processor Definitions
 ************************************************************************************/
/* Reference: Cortex-A53<99> MPCore Paragraph 4.2, "Register summary." */


/* Multiprocessor Affinity Register (MPIDR_EL1) */

#define MPIDR_CPUID_SHIFT	(0)       /* Bits 0-1: CPU ID */
#define MPIDR_CPUID_MASK	(3 << MPIDR_CPUID_SHIFT)
#define MPIDR_CPUID_CPU0	(0 << MPIDR_CPUID_SHIFT)
#define MPIDR_CPUID_CPU1	(1 << MPIDR_CPUID_SHIFT)
#define MPIDR_CPUID_CPU2	(2 << MPIDR_CPUID_SHIFT)
#define MPIDR_CPUID_CPU3	(3 << MPIDR_CPUID_SHIFT)
                                           /* Bits 2-7: Reserved */
#define MPIDR_CLUSTID_SHIFT      (8)       /* Bits 8-11: Cluster ID value */
#define MPIDR_CLUSTID_MASK       (15 << MPIDR_CLUSTID_SHIFT)
                                           /* Bits 12-29: Reserved */
#define MPIDR_U                  (1 << 30) /* Bit 30: Multiprocessing Extensions. */

/* Processor Feature Register 0 (ID_PFR0_EL1) */
/* TODO: To be provided */

/* Processor Feature Register 1 (ID_PFR1_EL1) */
/* TODO: To be provided */

/* Debug Feature Register 0 (ID_DFR0_EL1) */
/* TODO: To be provided */

/* Auxiliary Feature Register 0 (ID_AFR0_EL1) */
/* TODO: To be provided */

/* Memory Model Features Register 0 (ID_MMFR0_EL1) */
/* Memory Model Features Register 1 (ID_MMFR1_EL1) */
/* Memory Model Features Register 2 (ID_MMFR2_EL1) */
/* Memory Model Features Register 3 (ID_MMFR3_EL1) */
/* TODO: To be provided */

/* Instruction Set Attributes Register 0 (ID_ISAR0_EL1) */
/* Instruction Set Attributes Register 1 (ID_ISAR1_EL1) */
/* Instruction Set Attributes Register 2 (ID_ISAR2_EL1) */
/* Instruction Set Attributes Register 3 (ID_ISAR3_EL1) */
/* Instruction Set Attributes Register 4 (ID_ISAR4_EL1) */
/* Instruction Set Attributes Register 5 (ID_ISAR5_EL1) */
/* TODO: Others to be provided */

/* Cache Size Identification Register (CCSIDR_EL1) */
/* TODO: To be provided */

/* Cache Level ID Register (CLIDR_EL1) */
/* TODO: To be provided */

/* Auxiliary ID Register (AIDR_EL1) */
/* TODO: To be provided */

/* Cache Size Selection Register (CSSELR_EL1) */
/* TODO: To be provided */

/* System Control Register (SCTLR_EL1, SCTLR_EL2, SCTLR_EL3)
 *
 * NOTES:
 * (1) Always enabled on A53
 * (2) Not available on A53
 */

#define SCTLR_M			(1 << 0)  /* Bit 0:  Enables the MMU */
#define SCTLR_A			(1 << 1)  /* Bit 1:  Enables strict alignment of data */
#define SCTLR_C			(1 << 2)  /* Bit 2:  Determines if data can be cached */
#define SCTLR_SA		(1 << 3)  /* Bit 3:  Enables stack alignment check */
					  /* Bits 4-11: Reserved */
#define SCTLR_I			(1 << 12) /* Bit 12: Determines if instructions can be cached */
				/* Bits 13-18: Reserved */
#define SCTLR_WXN		(1 << 19) /* Bit 19: Force treatment of all memory regions with permissions as XN */
				/* Bits 20-24: Reserved */
#define SCTLR_EE		(1 << 25) /* Bit 25: Exception endianness */
					  /* Bits 24-63: Reserved */

/* Auxiliary Control Register (ACTLR_EL1, ACTLR_EL3, ACTLR_EL3) */

#define ACTLR_CPUACTLR		(1 << 0)  /* Bit 0: CPUACTLR_ELx write access control */
#define ACTLR_CPUECTLR		(1 << 1)  /* Bit 1: CPUECTLR_ELx write access control */
					  /* Bits 2-3: Reserved */
#define ACTLR_L2CTLR		(1 << 4)  /* Bit 4: L2CTLR_ELx write access control */
#define ACTLR_L2ECTLR		(1 << 5)  /* Bit 4: L2ECTLR_ELx write access control */
#define ACTLR_L2ACTLR		(1 << 6)  /* Bit 4: L2ACTLR_ELx write access control */
			  		  /* Bits 7-31: Reserved */

/* Coprocessor Access Control Register (CPACR_EL1) */
/* TODO: To be provided */

/* Secure Configuration Register (SCR_EL3) */
#define SCR_NS			(1 << 0)  /* Bit 0: Non-Secure bit */
#define SCR_IRQ			(1 << 1)  /* Bit 1: Physical IRQ Routing */
#define SCR_FIQ			(1 << 2)  /* Bit 2: Physical FIQ Routing */
#define SCR_EA			(1 << 3)  /* Bit 3: External Abort and SError interrupt Routing */
			  		  /* Bits 4-6: Reserved */
#define SCR_SMD			(1 << 7)  /* Bit 7: SMC instruction disable */
#define SCR_HCE			(1 << 8)  /* Bit 8: Hyp Call enable */
#define SCR_SIF			(1 << 9)  /* Bit 9: Secure Instruction Fetch */
#define SCR_RW			(1 << 10) /* Bit 10: Register width control for lower exception levels */
#define SCR_ST			(1 << 11) /* Bit 11: Enable Secure EL1 access to CNTPS_TVAL_EL1, CNTS_CTL_EL1, and CNTPS_CVAL_EL1 registers */
#define SCR_TWI			(1 << 12) /* Bit 12: Traps WFI instructions */
#define SCR_TWE			(1 << 13) /* Bit 13: Traps WFE instructions */
			  		  /* Bits 14-31: Reserved */

/* DAIF Interrupt Mask Bits */
			  		  /* Bits 0-5: Reserved */
#define DAIF_F			(1 << 6)  /* Bit 6: IRQ mask bit */
#define DAIF_I			(1 << 7)  /* Bit 7: FIQ mask bit */
#define DAIF_A			(1 << 8)  /* Bit 8: SError interrupt mask bit */
#define DAIF_D			(1 << 9)  /* Bit 9: Process state D mask bit */
			  		  /* Bits 10-31: Reserved */
/* PSTATE make bit */
#define PSTATE_F		(1 << 0) /* FIQ interrupt mask bit */
#define PSTATE_I		(1 << 1) /* IRQ interrupt mask bit */
#define PSTATE_A		(1 << 2) /* SError interrupt mask bit */
#define PSTATE_D		(1 << 3) /* Debug exception mask bit */

/* CurrentEL Current Exception Level */
#define CURRENTEL_MASK		(0b11 << 2) /* Current Exception level mask */
#define CURRENTEL_EL0		(0b00 << 2) /* EL0 */
#define CURRENTEL_EL1		(0b01 << 2) /* EL1 */
#define CURRENTEL_EL2		(0b10 << 2) /* EL2 */
#define CURRENTEL_EL3		(0b11 << 2) /* EL3 */

/* Secure Debug Enable Register (SDER32_EL3) */
/* TODO: To be provided */

/* Non-secure Access Control Register (NSACR) */

                                           /* Bits 0-9: Reserved */
#define NSACR_CP10               (1 << 10) /* Bit 10: Permission to access coprocessor 10 */
#define NSACR_CP11               (1 << 11) /* Bit 11: Permission to access coprocessor 11 */
                                           /* Bits 12-14: Reserved */
#define NSACR_NSASEDIS           (1 << 15) /* Bit 15: Disable Non-secure Advanced SIMD Extension */
                                           /* Bits 16-31: Reserved */

/* Translation Table Base Register 0 (TTBR0).  See mmu.h */
/* Translation Table Base Register 1 (TTBR1).  See mmu.h */
/* Translation Table Base Control Register (TTBCR).  See mmu.h */
/* Domain Access Control Register (DACR).  See mmu.h */
/* Data Fault Status Register (DFSR).  See mmu.h */
/* Instruction Fault Status Register (IFSR).  See mmu.h */

/* Auxiliary Data Fault Status Register (ADFSR).  Not used in this implementation. */

/* Data Fault Address Register(DFAR)
 *
 *   Holds the MVA of the faulting address when a synchronous fault occurs
 *
 * Instruction Fault Address Register(IFAR)
 *
 *   Holds the MVA of the faulting address of the instruction that caused a prefetch
 *   abort.
 *
 * NOP Register
 *
 *   The use of this register is optional and deprecated. Use the NOP instruction
 *   instead.
 *
 * Physical Address Register (PAR)
 *
 *   Holds:
 *   - the PA after a successful translation
 *   - the source of the abort for an unsuccessful translation
 *
 * Instruction Synchronization Barrier
 *
 *   The use of ISB is optional and deprecated. Use the instruction ISB instead.
 *
 * Data Memory Barrier
 *   The use of DMB is deprecated and, on Cortex-A53 MPCore, behaves as NOP. Use the
 *   instruction DMB instead.
 */

/* Vector Base Address Register (VBAR) */

#define VBAR_MASK                (0xfffff800)

/* Monitor Vector Base Address Register (MVBAR) */
/* TODO: To be provided */

/* Interrupt Status Register (ISR_EL1) */
                                          /* Bits 0-5: Reserved */
#define ISR_FIQ_PENDING          (1 << 6) /* Bit 5: FIQ pending bit */
#define ISR_IRQ_PENDING          (1 << 7) /* Bit 6: IRQ pending bit */
#define ISR_EA_PENDING           (1 << 8) /* Bit 7: External abort pending bit */
                                          /* Bits 9-31: Reserved */


/* Context ID Register (CONTEXTIDR_EL1) */

#define CONTEXTIDR_ASID_SHIFT    (0)   /* Bits 0-7: Address Space Identifier */
#define CONTEXTIDR_ASID_MASK     (0xff << CONTEXTIDR_ASID_SHIFT)
#define CONTEXTIDR_PROCID_SHIFT  (8) /* Bits 8-31: Process Identifier */
#define CONTEXTIDR_PROCID_MASK   (0x00ffffff << CONTEXTIDR_PROCID_SHIFT)

/* Configuration Base Address Register (CBAR) */
/* TODO: To be provided */

/************************************************************************************
 * Assemby Macros
 ************************************************************************************/

#ifdef __ASSEMBLY__

/* Get the device ID */

	.macro	rdid_el3, id
	mrs		\id, midr_el1
	.endm

/* Read/write the system control register (SCTLR) */

	.macro	rdsctlr_el3, sctlr
	mrs		\sctlr, sctlr_el3

	.endm

	.macro	wrsctlr_el3, sctlr
	msr		sctlr_el3, \sctlr
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	nop
	.endm
#endif /* __ASSEMBLY__ */

/************************************************************************************
 * Inline Functions
 ************************************************************************************/

#ifndef __ASSEMBLY__

/* Get the device ID register */

static inline unsigned int rdid_el3(void)
{
  unsigned int id;
  __asm__ __volatile__
    (
      "\tmrs %0, midr_el3\n"
      : "=r" (id)
      :
      : "memory"
    );

  return id;
}

/* Get the Multiprocessor Affinity Register (MPIDR_EL1) */

static inline unsigned int rdmpidr_el1(void)
{
  unsigned int mpidr;
  __asm__ __volatile__
    (
      "\tmrs %0, mpidr_el1\n"
      : "=r" (mpidr)
      :
      : "memory"
    );

  return mpidr;
}

/* Read/write the system control register (SCTLR_EL3) */

static inline unsigned int rdsctlr_el3(void)
{
  unsigned int sctlr;
  __asm__ __volatile__
    (
      "\tmrs %0, sctlr_el3\n"
      : "=r" (sctlr)
      :
      : "memory"
    );

  return sctlr;
}

static inline void wrsctlr_el3(unsigned int sctlr)
{
  __asm__ __volatile__
    (
      "\tmsr sctlr_el3, %0\n"
      "\tnop\n"
      "\tnop\n"
      "\tnop\n"
      "\tnop\n"
      "\tnop\n"
      "\tnop\n"
      "\tnop\n"
      "\tnop\n"
      :
      : "r" (sctlr)
      : "memory"
    );
}

/* Read/write the vector base address register (VBAR_EL3) */

static inline unsigned int rdvbar_el3(void)
{
  unsigned int sctlr;
  __asm__ __volatile__
    (
      "\tmrs %0, vbar_el3\n"
      : "=r" (sctlr)
      :
      : "memory"
    );

  return sctlr;
}

static inline void wrvbar_el3(unsigned int sctlr)
{
  __asm__ __volatile__
    (
      "\tmsr vbar_el3, %0\n"
      :
      : "r" (sctlr)
      : "memory"
    );
}

#endif /* __ASSEMBLY__ */

/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#ifndef __ASSEMBLY__
#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

#undef EXTERN
#ifdef __cplusplus
}
#endif
#endif /* __ASSEMBLY__ */

#endif  /* __ARCH_ARM_SRC_ARMV8_A_SCTLR_H */
