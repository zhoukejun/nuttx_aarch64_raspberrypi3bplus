/****************************************************************************
 * arch/arm/include/bcm2837b0/irq.h
 *
 *   Copyright 2019 NuttX. All rights reserved.
 *   Author: Kejun ZHOU <zhoukejun@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/* This file should never be included directed but, rather, only indirectly
 * through nuttx/irq.h
 */

#ifndef __ARCH_ARM_INCLUDE_BCM2837B0_IRQ_H
#define __ARCH_ARM_INCLUDE_BCM2837B0_IRQ_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/irq.h>

/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* IRQ channels */

#define BCM_BASIC_IRQ_ARM_TIMER			(0)	/* ARM Timer global interrupt */
#define BCM_BASIC_IRQ_ARM_MAILBOX		(1)	/* ARM Mailbox global interrupt */
#define BCM_BASIC_IRQ_ARM_DOORBELL_0		(2)	/* ARM Doorbell 0 global interrupt */
#define BCM_BASIC_IRQ_ARM_DOORBELL_1		(3)	/* ARM Doorbell 1 global interrupt */
#define BCM_BASIC_IRQ_GPU_0_HALTED		(4)	/* GPU 0 Halted global interrupt */
#define BCM_BASIC_IRQ_GPU_1_HALTED		(5)	/* GPU 1 Halted global interrupt */
#define BCM_BASIC_IRQ_ILLEGAL_ACC_TYPE_1	(6)	/* Illegal access type 1 global interrupt */
#define BCM_BASIC_IRQ_ILLEGAL_ACC_TYPE_0	(7)	/* Illegal access type 0 global interrupt */

#define BCM_BASIC_IRQ_PENDING_REG_1		(8)	/* One or more bits set in pending register 1 */
#define BCM_BASIC_IRQ_PENDING_REG_2		(9)	/* One or more bits set in pending register 2 */

#define BCM_BASIC_IRQ_GPU_IRQ_7			(10)
#define BCM_BASIC_IRQ_GPU_IRQ_9			(11)
#define BCM_BASIC_IRQ_GPU_IRQ_10		(12)
#define BCM_BASIC_IRQ_GPU_IRQ_18		(13)
#define BCM_BASIC_IRQ_GPU_IRQ_19		(14)
#define BCM_BASIC_IRQ_GPU_IRQ_53		(15)
#define BCM_BASIC_IRQ_GPU_IRQ_54		(16)
#define BCM_BASIC_IRQ_GPU_IRQ_55		(17)
#define BCM_BASIC_IRQ_GPU_IRQ_56		(18)
#define BCM_BASIC_IRQ_GPU_IRQ_57		(19)
#define BCM_BASIC_IRQ_GPU_IRQ_62		(20)

#define BCM_BASIC_IRQ_TOTAL			(21) 

#define BCM_PERIPHERALS_IRQ_SYS_TIMER_MATCH_1	(1 + BCM_BASIC_IRQ_TOTAL)
#define BCM_PERIPHERALS_IRQ_SYS_TIMER_MATCH_3	(3 + BCM_BASIC_IRQ_TOTAL)
#define BCM_PERIPHERALS_IRQ_USB			(9 + BCM_BASIC_IRQ_TOTAL)
#define BCM_PERIPHERALS_IRQ_AUX_INT		(29 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_I2C_SPI_SLV_INT	(43 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_PWA_0		(45 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_PWA_1		(46 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_SMI			(48 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_GPIO_INT_0		(49 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_GPIO_INT_1		(50 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_GPIO_INT_2		(51 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_GPIO_INT_3		(52 + BCM_BASIC_IRQ_TOTAL)
#define BCM_PERIPHERALS_IRQ_I2C_INT		(53 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_SPI_INT		(54 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_PCM_INT		(55 + BCM_BASIC_IRQ_TOTAL) 
#define BCM_PERIPHERALS_IRQ_UART_INT		(57 + BCM_BASIC_IRQ_TOTAL) 

#define BCM_PERIPHERALS_IRQ_TOTAL		(64)

#define NR_IRQS			(BCM_BASIC_IRQ_TOTAL + BCM_PERIPHERALS_IRQ_TOTAL)	/* Total number of interrupts */

#define BCM_IRQ_SYSTIMER	BCM_BASIC_IRQ_ARM_TIMER

/* FIQ channels */ 

#define	BCM_FIQ_GPU_0				(0)	/* GPU Interrupti 0 */
#define	BCM_FIQ_GPU_1				(1)	/* GPU Interrupti 1 */
#define BCM_FIQ_ARM_TIMER			(64)	/* FIQ ARM Timer global interrupt */
#define BCM_FIQ_ARM_MAILBOX			(65)	/* FIQ ARM Mailbox global interrupt */
#define BCM_FIQ_ARM_DOORBELL_0			(66)	/* FIQ ARM Doorbell 0 global interrupt */
#define BCM_FIQ_ARM_DOORBELL_1			(67)	/* FIQ ARM Doorbell 1 global interrupt */
#define BCM_FIQ_GPU_0_HALTED			(68)	/* FIQ GPU 0 Halted global interrupt */
#define BCM_FIQ_GPU_1_HALTED			(69)	/* FIQ GPU 1 Halted global interrupt */
#define BCM_FIQ_ILLEGAL_ACC_TYPE_1		(70)	/* FIQ Illegal access type 1 global interrupt */
#define BCM_FIQ_ILLEGAL_ACC_TYPE_0		(71)	/* FIQ Illegal access type 0 global interrupt */


/****************************************************************************
 * Public Types
 ****************************************************************************/

/****************************************************************************
 * Public Types
 ****************************************************************************/


/****************************************************************************
 * Public Data
 ****************************************************************************/

/****************************************************************************
 * Public Functions Prototype
 ****************************************************************************/


#ifndef __ASSEMBLY__
#ifdef __cplusplus
#define EXTERN extern "C"
extern "C" {
#else
#define EXTERN extern
#endif

#undef EXTERN
#ifdef __cplusplus
}
#endif
#endif

#endif /* __ARCH_ARM_INCLUDE_BCM2837B0_IRQ_H */
