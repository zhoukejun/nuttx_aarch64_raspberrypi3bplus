/****************************************************************************
 * arch/arm/include/armv8-a/irq.h
 *
 *   Copyright (C) 2013-2014, 2018 Gregory Nutt. All rights reserved.
 *   Author: Gregory Nutt <gnutt@nuttx.org>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 * 3. Neither the name NuttX nor the names of its contributors may be
 *    used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/

/* This file should never be included directed but, rather, only indirectly
 * through nuttx/irq.h
 */

#ifndef __ARCH_ARM_INCLUDE_ARMV8_A_IRQ_H
#define __ARCH_ARM_INCLUDE_ARMV8_A_IRQ_H

/****************************************************************************
 * Included Files
 ****************************************************************************/

#include <nuttx/config.h>
#include <nuttx/irq.h>

#ifndef __ASSEMBLY__
#  include <stdint.h>
#  include <arch/arch.h>
#endif

#if 0
/* PSTATE make bit */
#define PSTATE_F	1 //	(1 << 0) /* FIQ interrupt mask bit */
#define PSTATE_I	2 //	(1 << 1) /* IRQ interrupt mask bit */
#define PSTATE_A	4 //	(1 << 2) /* SError interrupt mask bit */
#define PSTATE_D	8 //	(1 << 3) /* Debug exception mask bit */
#endif
/****************************************************************************
 * Pre-processor Definitions
 ****************************************************************************/

/* IRQ Stack Frame Format:
 *
 * Context is always saved/restored in the same way:
 *
 *   (1) x0-x32(xzr)
 *   (2) then the PC and CPSR
 *
 * This results in the following set of indices that can be used to access
 * individual registers in the xcp.regs array:
 */

#define REG_X0              (0)
#define REG_X1              (1)
#define REG_X2              (2)
#define REG_X3              (3)
#define REG_X4              (4)
#define REG_X5              (5)
#define REG_X6              (6)
#define REG_X7              (7)
#define REG_X8              (8)
#define REG_X9              (9)
#define REG_X10             (10)
#define REG_X11             (11)
#define REG_X12             (12)
#define REG_X13             (13)
#define REG_X14             (14)
#define REG_X15             (15)
#define REG_X16             (16)
#define REG_X17             (17)
#define REG_X18             (18)
#define REG_X19             (19)
#define REG_X20             (20)
#define REG_X21             (21)
#define REG_X22             (22)
#define REG_X23             (23)
#define REG_X24             (24)
#define REG_X25             (25)
#define REG_X26             (26)
#define REG_X27             (27)
#define REG_X28             (28)
#define REG_X29             (29)
#define REG_X30             (30)
#define REG_XZR             (31)
#define REG_ELR_EL3         (32)
#define REG_SPSR_EL3        (33)

#define ARM_CONTEXT_REGS    (34)

/* If the MCU supports a floating point unit, then it will be necessary
 * to save the state of the FPU status register and data registers on
 * each context switch.  These registers are not saved during interrupt
 * level processing, however. So, as a consequence, floating point
 * operations may NOT be performed in interrupt handlers.
 *
 * The FPU provides an extension register file containing 32 single-
 * precision registers. These can be viewed as:
 *
 * - Sixteen 128-bit double word registers, Q0-Q15
 * - Thirty-two 32-bit single-word registers, S0-S31
 *   S<2n> maps to the least significant half of D<n>
 *   S<2n+1> maps to the most significant half of D<n>.
 */

#ifdef CONFIG_ARCH_FPU
#  define REG_Q0	(ARM_CONTEXT_REGS+0)  /* D0 */
#  define REG_Q1	(ARM_CONTEXT_REGS+1)  /* D1 */
#  define REG_Q2	(ARM_CONTEXT_REGS+2)  /* D2 */
#  define REG_Q3	(ARM_CONTEXT_REGS+3)  /* D3 */
#  define REG_Q4	(ARM_CONTEXT_REGS+4)  /* D4 */
#  define REG_Q5	(ARM_CONTEXT_REGS+5)  /* D5 */
#  define REG_Q6	(ARM_CONTEXT_REGS+6)  /* D6 */
#  define REG_Q7	(ARM_CONTEXT_REGS+7)  /* D7 */
#  define REG_Q8	(ARM_CONTEXT_REGS+8)  /* D8 */
#  define REG_Q9	(ARM_CONTEXT_REGS+9)  /* D9 */
#  define REG_Q10	(ARM_CONTEXT_REGS+10)  /* D10 */
#  define REG_Q11	(ARM_CONTEXT_REGS+11)  /* D11 */
#  define REG_Q12	(ARM_CONTEXT_REGS+12)  /* D12 */
#  define REG_Q13	(ARM_CONTEXT_REGS+13)  /* D13 */
#  define REG_Q14	(ARM_CONTEXT_REGS+14)  /* D14 */
#  define REG_Q15	(ARM_CONTEXT_REGS+15)  /* D15 */
#  define REG_Q16	(ARM_CONTEXT_REGS+16)  /* D16 */
#  define REG_Q17	(ARM_CONTEXT_REGS+17)  /* D17 */
#  define REG_Q18	(ARM_CONTEXT_REGS+18)  /* D18 */
#  define REG_Q19	(ARM_CONTEXT_REGS+19)  /* D19 */
#  define REG_Q20	(ARM_CONTEXT_REGS+20)  /* D20 */
#  define REG_Q21	(ARM_CONTEXT_REGS+21)  /* D21 */
#  define REG_Q22	(ARM_CONTEXT_REGS+22)  /* D22 */
#  define REG_Q23	(ARM_CONTEXT_REGS+23)  /* D23 */
#  define REG_Q24	(ARM_CONTEXT_REGS+24)  /* D24 */
#  define REG_Q25	(ARM_CONTEXT_REGS+25)  /* D25 */
#  define REG_Q26	(ARM_CONTEXT_REGS+26)  /* D26 */
#  define REG_Q27	(ARM_CONTEXT_REGS+27)  /* D27 */
#  define REG_Q28	(ARM_CONTEXT_REGS+28)  /* D28 */
#  define REG_Q29	(ARM_CONTEXT_REGS+29)  /* D29 */
#  define REG_Q30	(ARM_CONTEXT_REGS+30)  /* D30 */
#  define REG_Q31	(ARM_CONTEXT_REGS+31)  /* D31 */

#  define REG_FPCR	(ARM_CONTEXT_REGS+32) /* Floating point control */
#  define REG_FPSR	(ARM_CONTEXT_REGS+33) /* Floating point status */

#  define FPU_CONTEXT_REGS  (34)
#else
#  define FPU_CONTEXT_REGS  (0)
#endif

/* The total number of registers saved by software */

#define XCPTCONTEXT_REGS    (ARM_CONTEXT_REGS + FPU_CONTEXT_REGS)
#define XCPTCONTEXT_SIZE    (4 * XCPTCONTEXT_REGS)

/****************************************************************************
 * Public Types
 ****************************************************************************/

#ifndef __ASSEMBLY__

/* This structure represents the return state from a system call */

#ifdef CONFIG_LIB_SYSCALL
struct xcpt_syscall_s
{
#ifdef CONFIG_BUILD_KERNEL
  uint64_t spsr_el3;        /* The  value */
#endif
  uint32_t elr_el3;   /* The return PC */
};
#endif

/* This struct defines the way the registers are stored.  We need to save:
 *
 *  1  CPSR
 *  7  Static registers, v1-v7 (aka r4-r10)
 *  1  Frame pointer, fp (aka r11)
 *  1  Stack pointer, sp (aka r13)
 *  1  Return address, lr (aka r14)
 * ---
 * 11  (XCPTCONTEXT_USER_REG)
 *
 * On interrupts, we also need to save:
 *  4  Volatile registers, a1-a4 (aka r0-r3)
 *  1  Scratch Register, ip (aka r12)
 *---
 *  5  (XCPTCONTEXT_IRQ_REGS)
 *
 * For a total of 17 (XCPTCONTEXT_REGS)
 */

#ifndef __ASSEMBLY__
struct xcptcontext
{
#ifndef CONFIG_DISABLE_SIGNALS
  /* The following function pointer is non-zero if there are pending signals
   * to be processed.
   */

  void *sigdeliver; /* Actual type is sig_deliver_t */

  /* These are saved copies of LR and CPSR used during signal processing.
   *
   * REVISIT:  Because there is only one copy of these save areas,
   * only a single signal handler can be active.  This precludes
   * queuing of signal actions.  As a result, signals received while
   * another signal handler is executing will be ignored!
   */

  uint64_t saved_elr_el3;
  uint64_t saved_spsr_el3;

# ifdef CONFIG_BUILD_KERNEL
  /* This is the saved address to use when returning from a user-space
   * signal handler.
   */

  uint32_t sigreturn;

# endif
#endif

  /* Register save area */

  uint64_t regs[XCPTCONTEXT_REGS];

  /* Extra fault address register saved for common paging logic.  In the
   * case of the pre-fetch abort, this value is the same as regs[REG_R15];
   * For the case of the data abort, this value is the value of the fault
   * address register (FAR) at the time of data abort exception.
   */

#ifdef CONFIG_PAGING
  uintptr_t far;
#endif

#ifdef CONFIG_LIB_SYSCALL
  /* The following array holds the return address and the exc_return value
   * needed to return from each nested system call.
   */

  uint8_t nsyscalls;
  struct xcpt_syscall_s syscall[CONFIG_SYS_NNEST];
#endif

#ifdef CONFIG_ARCH_ADDRENV
#ifdef CONFIG_ARCH_STACK_DYNAMIC
  /* This array holds the physical address of the level 2 page table used
   * to map the thread's stack memory.  This array will be initially of
   * zeroed and would be back-up up with pages during page fault exception
   * handling to support dynamically sized stacks for each thread.
   */

  FAR uintptr_t *ustack[ARCH_STACK_NSECTS];
#endif

#ifdef CONFIG_ARCH_KERNEL_STACK
  /* In this configuration, all syscalls execute from an internal kernel
   * stack.  Why?  Because when we instantiate and initialize the address
   * environment of the new user process, we will temporarily lose the
   * address environment of the old user process, including its stack
   * contents.  The kernel C logic will crash immediately with no valid
   * stack in place.
   */

  FAR uint64_t *ustkptr;  /* Saved user stack pointer */
  FAR uint64_t *kstack;   /* Allocate base of the (aligned) kernel stack */
#ifndef CONFIG_DISABLE_SIGNALS
  FAR uint64_t *kstkptr;  /* Saved kernel stack pointer */
#endif
#endif
#endif
};
#endif

#endif /* __ASSEMBLY__ */

/****************************************************************************
 * Inline functions
 ****************************************************************************/

#ifndef __ASSEMBLY__

/* Name: up_irq_save, up_irq_restore, and friends.
 *
 * NOTE: This function should never be called from application code and,
 * as a general rule unless you really know what you are doing, this
 * function should not be called directly from operation system code either:
 * Typically, the wrapper functions, enter_critical_section() and
 * leave_critical section(), are probably what you really want.
 */


static inline unsigned long up_irq_save(void)
{
  unsigned long flag;

  __asm__ __volatile__
    (
      "\tmrs    %0, daif\n"
      "\tmsr    daifset, #2\n"
#if defined(CONFIG_ARMV8A_DECODEFIQ)
      "\tmsr    daifset, #1\n"
#endif
      : "=r" (flag)
      :
      : "memory"
    );

  return flag;
}


static inline void up_irq_enable(void)
{
  __asm__ __volatile__
    (
      "\tmsr    daifclr, #2\n"
#if defined(CONFIG_ARMV8A_DECODEFIQ)
      "\tmsr    daifclr, #1\n"
#endif
      :
      :
      : "memory"
    );
}

/* Restore saved IRQ & FIQ state */

static inline void up_irq_disable(void)
{
  __asm__ __volatile__
    (
      "\tmsr    daifset, #2\n"
#if defined(CONFIG_ARMV8A_DECODEFIQ)
      "\tmsr    daifset, #1\n"
#endif
      :
      :
      : "memory"
    );
}

/* Restore saved IRQ & FIQ state */

static inline void up_irq_restore(irqstate_t flags)
{
  __asm__ __volatile__
    (
      "\tmsr    daif, %0\n"
      :
      : "r" (flags)
      : "memory"
    );
}

#endif /* __ASSEMBLY__ */

/****************************************************************************
 * Public Data
 ****************************************************************************/

#ifndef __ASSEMBLY__
#ifdef __cplusplus
#define EXTERN extern "C"
extern "C"
{
#else
#define EXTERN extern
#endif

/****************************************************************************
 * Public Function Prototypes
 ****************************************************************************/

#undef EXTERN
#ifdef __cplusplus
}
#endif
#endif

#endif /* __ARCH_ARM_INCLUDE_ARMV8_A_IRQ_H */
